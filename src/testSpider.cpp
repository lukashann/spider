#include "spider.h"

#include <pigpio.h>
#include <chrono>
#include <thread>
#include <iostream>
#include <signal.h>
#include <memory>
#include <Eigen/Dense>

uint8_t sigTermReceived = 0;

void stopRunning(int signal)
{
	std::cout << "sigTerm received" << std::endl;
	sigTermReceived = 1;
}

void forwardBackward(std::shared_ptr<spider::Spider> spooda)
{
    constexpr float xMin = -0.03;
    constexpr float xMax = 0.03;
    float curX = 0;
	float inc = 0.0005; // [m]
	constexpr int deltaT = 10; // [ms]
    Eigen::Matrix4f targetPose = Eigen::Matrix4f::Identity();

    while( sigTermReceived == 0 )
    {
        if (inc > 0 )
		{
			if (curX < xMax)
			{
				curX += inc;
			}
			else
			{
				inc = -inc;
			}
		}
		else
		{
  			if (curX > xMin)
			{
				curX += inc;
			}
			else
			{
				inc = -inc;
			}
		}

        // z-component
        targetPose(0, 3) = curX;
        spooda->moveToPose(targetPose);

        std::this_thread::sleep_for(std::chrono::milliseconds(deltaT));
    }

}

void sideToSide(std::shared_ptr<spider::Spider> spooda)
{
    constexpr float yMin = -0.03;
    constexpr float yMax = 0.03;
    float curY = 0;
	float inc = 0.0005; // [m]
	constexpr int deltaT = 10; // [ms]
    Eigen::Matrix4f targetPose = Eigen::Matrix4f::Identity();

    while( sigTermReceived == 0 )
    {
        if (inc > 0 )
		{
			if (curY < yMax)
			{
				curY += inc;
			}
			else
			{
				inc = -inc;
			}
		}
		else
		{
  			if (curY > yMin)
			{
				curY += inc;
			}
			else
			{
				inc = -inc;
			}
		}

        // z-component
        targetPose(1, 3) = curY;
        spooda->moveToPose(targetPose);

        std::this_thread::sleep_for(std::chrono::milliseconds(deltaT));
    }

}

void upDown(std::shared_ptr<spider::Spider> spooda)
{
    constexpr float minHeight = -0.03;
    constexpr float maxHeight = 0.03;
    float curHeight = 0;
	float inc = 0.0005; // [m]
	constexpr int deltaT = 10; // [ms]
    Eigen::Matrix4f targetPose = Eigen::Matrix4f::Identity();

    while( sigTermReceived == 0 )
    {
        if (inc > 0 )
		{
			if (curHeight < maxHeight)
			{
				curHeight += inc;
			}
			else
			{
				inc = -inc;
			}
		}
		else
		{
  			if (curHeight > minHeight)
			{
				curHeight += inc;
			}
			else
			{
				inc = -inc;
			}
		}

        // z-component
        targetPose(2, 3) = curHeight;
        spooda->moveToPose(targetPose);

        std::this_thread::sleep_for(std::chrono::milliseconds(deltaT));
    }

}

int main(int argc, char* argv[])
{
    std::shared_ptr<spider::Spider> spooda = std::make_shared<spider::Spider>();

    gpioSetSignalFunc(SIGINT, stopRunning);

    forwardBackward(spooda);

    spooda->disable();

    return 0;
}
