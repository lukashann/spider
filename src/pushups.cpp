#include "CPca9685.h"
#include "servo.h"
#include "joint.h"
#include "leg.h"

#include "spdlog/spdlog.h"

#include <pigpio.h>

#include <signal.h>
#include <array>
#include <cstdint>
#include <math.h>
#include <chrono>
#include <thread>
#include <iostream>

/*
float RzRight = -HALF_PI;
float RzLeft = HALF_PI;
BLA::Matrix<4, 4> T0FrontRight = {  cos(RzRight), -sin(RzRight), 0, 0.033,
                                    sin(RzRight), cos(RzRight), 0, -0.033,
                                    0, 0, 1, 0,
                                    0, 0, 0, 1};
BLA::Matrix<4, 4> T0RearRight = {  cos(RzRight), -sin(RzRight), 0, -0.033,
                                   sin(RzRight), cos(RzRight), 0, -0.033,
                                   0, 0, 1, 0,
                                   0, 0, 0, 1};
BLA::Matrix<4, 4> T0RearLeft = {  cos(RzLeft), -sin(RzLeft), 0, -0.033,
                                  sin(RzLeft), cos(RzLeft), 0, 0.033,
                                  0, 0, 1, 0,
                                  0, 0, 0, 1};
BLA::Matrix<4, 4> T0FrontLeft = {  cos(RzLeft), -sin(RzLeft), 0, 0.033,
                                   sin(RzLeft), cos(RzLeft), 0, 0.033,
                                   0, 0, 1, 0,
                                   0, 0, 0, 1};            

BLA::Matrix<4, 4> identity = {  1, 0, 0, 0,
                                0, 1, 0, 0,
                                0, 0, 1, 0,
                                0, 0, 0, 1  };                                                                                         

*/
           
uint8_t sigTermReceived = 0;

void stopRunning(int signal)
{
	std::cout << "sigTerm received" << std::endl;
	sigTermReceived = 1;
}
	
int main (int argc, char* argv[]) {

	// we are currently setting position wrt base of each leg, not the robot
	std::shared_ptr<i2cPwmController::II2cPwmController> pwmController = std::make_shared<i2cPwmController::CPca9685>(1, 0x40);
	pwmController->setFrequency(50);

	gpioSetSignalFunc(SIGINT, stopRunning);

	for(uint8_t i = 0; i < 12; ++i)
	{
		pwmController->setDutyCycle(i, 0.075);
	}
	pwmController->setActive(true);


	std::array<uint8_t, 4> basePin = {0, 3, 6, 9};
	std::array<uint8_t, 4> hipPin = {1, 4, 7, 10};
	std::array<uint8_t, 4> kneePin = {2, 5, 8, 11};

	std::array<std::unique_ptr<servo::Servo>, 4> baseServo = {	std::make_unique<servo::Servo>("Base FR", pwmController, basePin[0]), 
									std::make_unique<servo::Servo>("Base RR", pwmController, basePin[1]), 
									std::make_unique<servo::Servo>("Base RL", pwmController, basePin[2]), 
									std::make_unique<servo::Servo>("Base FL", pwmController, basePin[3])  };
	std::array<std::unique_ptr<servo::Servo>, 4> hipServo = {	std::make_unique<servo::Servo>("Hip FR", pwmController, hipPin[0]), 
									std::make_unique<servo::Servo>("Hip RR", pwmController, hipPin[1]), 
									std::make_unique<servo::Servo>("Hip RL", pwmController, hipPin[2]), 
									std::make_unique<servo::Servo>("Hip FL", pwmController, hipPin[3])  };
	std::array<std::unique_ptr<servo::Servo>, 4> kneeServo = {	std::make_unique<servo::Servo>("Knee FR", pwmController, kneePin[0]), 
									std::make_unique<servo::Servo>("Knee RR", pwmController, kneePin[1]), 
									std::make_unique<servo::Servo>("Knee RL", pwmController, kneePin[2]), 
									std::make_unique<servo::Servo>("Knee FL", pwmController, kneePin[3])  };
	//baseServo[0]->setLogLevel(spdlog::level::level_enum::trace);
	//hipServo[0]->setLogLevel(spdlog::level::level_enum::trace);
	//kneeServo[0]->setLogLevel(spdlog::level::level_enum::trace);

	std::array<std::unique_ptr<joint::Joint>, 4> baseJoint = {	std::make_unique<joint::Joint>("Base FR", std::move(baseServo[0]), false, 0, -0.06),
									std::make_unique<joint::Joint>("Base RR", std::move(baseServo[1]), false, 0, 0.16),
									std::make_unique<joint::Joint>("Base RL", std::move(baseServo[2]), false, 0, 0.12),
									std::make_unique<joint::Joint>("Base FL", std::move(baseServo[3]), false, 0, 0.22) };
	std::array<std::unique_ptr<joint::Joint>, 4> hipJoint = {	std::make_unique<joint::Joint>("Hip FR", std::move(hipServo[0]), false, 0, 0.1),
									std::make_unique<joint::Joint>("Hip RR", std::move(hipServo[1]), true, 0, 0),
									std::make_unique<joint::Joint>("Hip RL", std::move(hipServo[2]), false, 0, 0.18),
									std::make_unique<joint::Joint>("Hip FL", std::move(hipServo[3]), true, 0, -0.08) };
	std::array<std::unique_ptr<joint::Joint>, 4> kneeJoint = {	std::make_unique<joint::Joint>("Knee FR", std::move(kneeServo[0]), true, M_PI_2, -0.27),
									std::make_unique<joint::Joint>("Knee RR", std::move(kneeServo[1]), false, -M_PI_2, 0.17),
									std::make_unique<joint::Joint>("Knee RL", std::move(kneeServo[2]), true, M_PI_2, -0.08),
									std::make_unique<joint::Joint>("Knee FL", std::move(kneeServo[3]), false, -M_PI_2, 0.1) };
	//baseJoint[0]->setLogLevel(spdlog::level::level_enum::trace);
	//hipJoint[0]->setLogLevel(spdlog::level::level_enum::trace);
	//kneeJoint[0]->setLogLevel(spdlog::level::level_enum::trace);
	
	std::array<std::unique_ptr<joint::Joint>, 3> leg0 = { std::move(baseJoint[0]), std::move(hipJoint[0]), std::move(kneeJoint[0]) };
	std::array<std::unique_ptr<joint::Joint>, 3> leg1 = { std::move(baseJoint[1]), std::move(hipJoint[1]), std::move(kneeJoint[1]) };
	std::array<std::unique_ptr<joint::Joint>, 3> leg2 = { std::move(baseJoint[2]), std::move(hipJoint[2]), std::move(kneeJoint[2]) };
	std::array<std::unique_ptr<joint::Joint>, 3> leg3 = { std::move(baseJoint[3]), std::move(hipJoint[3]), std::move(kneeJoint[3]) };

	std::array<std::array<std::unique_ptr<joint::Joint>, 3>, 4> legJoints = { std::move(leg0), std::move(leg1), std::move(leg2), std::move(leg3) };

	std::array<std::unique_ptr<leg::Leg>, 4> leg = {	std::make_unique<leg::Leg>("FR", std::move(legJoints[0]), Eigen::Matrix4f::Identity()), 
								std::make_unique<leg::Leg>("RR", std::move(legJoints[1]), Eigen::Matrix4f::Identity()), 
								std::make_unique<leg::Leg>("RL", std::move(legJoints[2]), Eigen::Matrix4f::Identity()), 
								std::make_unique<leg::Leg>("FL", std::move(legJoints[3]), Eigen::Matrix4f::Identity()) };

	//leg[0]->setLogLevel(spdlog::level::level_enum::trace);
	
	float curHeight = 0.055;
	float inc = 0.0005; // [m]
	int deltaT = 10; // [ms]
	// -> 1 cm/s
	float minHeight = 0.06;
	float maxHeight = 0.12;
	// front left leg is 0, rear left leg is 1, fear right leg is 2, front right leg is 3

	while(sigTermReceived == 0)
	{
		if (inc > 0 )
		{
			if (curHeight < maxHeight)
			{
				curHeight += inc;
			}
			else
			{
				inc = -inc;
			}
		}
		else
		{
  			if (curHeight > minHeight)
			{
				curHeight += inc;
			}
			else
			{
				inc = -inc;
			}
		}
  
		leg[0]->setPos(0.04, 0.0, -curHeight);
		leg[1]->setPos(0.04, 0.0, -curHeight);
		leg[2]->setPos(0.04, 0.0, -curHeight);
		leg[3]->setPos(0.04, 0.0, -curHeight);

		// min height: -0.035
		// max height: -0.12
		//
		//leg[0]->setPos(0.04, 0.0, -0.1);

		std::this_thread::sleep_for(std::chrono::milliseconds(deltaT));
	}

	pwmController->setActive(false);

}
