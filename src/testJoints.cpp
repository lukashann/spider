#include "joint.h"
#include "servo.h"
#include "CPca9685.h"

#include <cstdint>
#include <chrono>
#include <thread>
#include <math.h>
#include <memory>
#include <string>
#include <cmath>
#include <signal.h>
#include <array>
#include <pigpio.h>

#include "tclap/CmdLine.h"

uint8_t sigTermReceived = 0;

void stopRunning(int signal)
{
	std::cout << "sigTerm received" << std::endl;
	sigTermReceived = 1;
}

int main(int argc, char* argv[])
{
	float baseJointTargetAngle = 0;
	float hipJointTargetAngle = 0;
	float kneeJointTargetAngle = M_PI_2;
	// TCLAP doesn't like uint8_t as a template argument for some reason
	int targetLeg = 0;
	bool verbose = false;

	try
	{
		TCLAP::CmdLine cmdLine("Test Program for the joint class", ' ', "0.1");

		std::string description = "Target angle for base joint";
		TCLAP::ValueArg<float> baseJointArg("", "base", description, false, baseJointTargetAngle, "float");
		cmdLine.add(baseJointArg);

		description = "Target angle for hip joint";
		TCLAP::ValueArg<float> hipJointArg("", "hip", description, false, hipJointTargetAngle, "float");
		cmdLine.add(hipJointArg);

		description = "Target angle for knee joint";
		TCLAP::ValueArg<float> kneeJointArg("", "knee", description, false, kneeJointTargetAngle, "float");
		cmdLine.add(kneeJointArg);

		description = "Target leg to control";
		TCLAP::ValueArg<int> targetLegArg("", "leg", description, true, targetLeg, "uint8_t");
	       	cmdLine.add(targetLegArg);

		description = "verbose output";
		TCLAP::ValueArg<bool> verbosityArg("v", "verbose", description, false, verbose, "bool");
		cmdLine.add(verbosityArg);

		cmdLine.parse(argc, argv);

		baseJointTargetAngle = baseJointArg.getValue();
		hipJointTargetAngle = hipJointArg.getValue();
		kneeJointTargetAngle = kneeJointArg.getValue();
		targetLeg = uint8_t(targetLegArg.getValue());
		verbose = verbosityArg.getValue();

	}
	catch (TCLAP::ArgException &e)  // catch exceptions
	{ 
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
	}

	std::shared_ptr<i2cPwmController::II2cPwmController> pwmController = std::make_shared<i2cPwmController::CPca9685>(1, 0x40);
	pwmController->setFrequency(50);
	pwmController->setActive(true);

	gpioSetSignalFunc(SIGINT, stopRunning);


	std::array<uint8_t, 4> basePin = {0, 3, 6, 9};
	std::array<uint8_t, 4> hipPin = {1, 4, 7, 10};
	std::array<uint8_t, 4> kneePin = {2, 5, 8, 11};

	std::array<std::unique_ptr<servo::Servo>, 4> baseServo = {	std::make_unique<servo::Servo>("Base FR", pwmController, basePin[0]),
									std::make_unique<servo::Servo>("Base RR", pwmController, basePin[1]),
									std::make_unique<servo::Servo>("Base RL", pwmController, basePin[2]),
									std::make_unique<servo::Servo>("Base FL", pwmController, basePin[3])  };
	std::array<std::unique_ptr<servo::Servo>, 4> hipServo = {	std::make_unique<servo::Servo>("Hip FR", pwmController, hipPin[0]),
									std::make_unique<servo::Servo>("Hip RR", pwmController, hipPin[1]),
									std::make_unique<servo::Servo>("Hip RL", pwmController, hipPin[2]),
									std::make_unique<servo::Servo>("Hip FL", pwmController, hipPin[3])  };
	std::array<std::unique_ptr<servo::Servo>, 4> kneeServo = {	std::make_unique<servo::Servo>("Knee FR", pwmController, kneePin[0]),
									std::make_unique<servo::Servo>("Knee RR", pwmController, kneePin[1]),
									std::make_unique<servo::Servo>("Knee RL", pwmController, kneePin[2]),
									std::make_unique<servo::Servo>("Knee FL", pwmController, kneePin[3])  };
	if (verbose)
	{
		baseServo[targetLeg]->setLogLevel(spdlog::level::level_enum::trace);
		hipServo[targetLeg]->setLogLevel(spdlog::level::level_enum::trace);
		kneeServo[targetLeg]->setLogLevel(spdlog::level::level_enum::trace);
	}

	std::array<std::unique_ptr<joint::Joint>, 4> baseJoint = {	std::make_unique<joint::Joint>("Base FR", std::move(baseServo[0]), false, 0, -0.06),
									std::make_unique<joint::Joint>("Base RR", std::move(baseServo[1]), false, 0, 0.16),
									std::make_unique<joint::Joint>("Base RL", std::move(baseServo[2]), false, 0, 0.12),
									std::make_unique<joint::Joint>("Base FL", std::move(baseServo[3]), false, 0, 0.22) };
	std::array<std::unique_ptr<joint::Joint>, 4> hipJoint = {	std::make_unique<joint::Joint>("Hip FR", std::move(hipServo[0]), false, 0, 0.1),
									std::make_unique<joint::Joint>("Hip RR", std::move(hipServo[1]), true, 0, 0),
									std::make_unique<joint::Joint>("Hip RL", std::move(hipServo[2]), false, 0, 0.18),
									std::make_unique<joint::Joint>("Hip FL", std::move(hipServo[3]), true, 0, -0.08) };
	std::array<std::unique_ptr<joint::Joint>, 4> kneeJoint = {	std::make_unique<joint::Joint>("Knee FR", std::move(kneeServo[0]), true, M_PI_2, -0.27),
									std::make_unique<joint::Joint>("Knee RR", std::move(kneeServo[1]), false, -M_PI_2, 0.17),
									std::make_unique<joint::Joint>("Knee RL", std::move(kneeServo[2]), true, M_PI_2, -0.08),
									std::make_unique<joint::Joint>("Knee FL", std::move(kneeServo[3]), false, -M_PI_2, 0.1) };
	if (verbose)
	{
		baseJoint[targetLeg]->setLogLevel(spdlog::level::level_enum::trace);
		hipJoint[targetLeg]->setLogLevel(spdlog::level::level_enum::trace);
		kneeJoint[targetLeg]->setLogLevel(spdlog::level::level_enum::trace);
	}

	//std::cout << "targetLeg: " << targetLeg << std::endl;
	//std::cout << "targetLeg==0: " << targetLeg==0 << std::endl;
	//std::cout << "ptr[0]: " << baseJoint[0].get() << std::endl;
	//std::cout << "ptr[targetLeg]: " << baseJoint[targetLeg].get() << std::endl;

	while(sigTermReceived == 0)
	{
		baseJoint[targetLeg]->setAngle(baseJointTargetAngle);
		hipJoint[targetLeg]->setAngle(hipJointTargetAngle);
		kneeJoint[targetLeg]->setAngle(kneeJointTargetAngle);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	pwmController->setActive(false);


}
