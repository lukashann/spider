#include "servo.h"
#include "CPca9685.h"

#include "tclap/CmdLine.h"

#include <signal.h>
#include <pigpio.h>
#include <iostream>
#include <memory>
#include <array>
#include <cstdint>
#include <string>
#include <chrono>
#include <thread>

uint8_t sigTermReceived = 0;

void stopRunning(int signal)
{
	std::cout << "sigTerm received" << std::endl;
	sigTermReceived = 1;
}

int main(int argc, char* argv[])
{
    // TCLAP does not like uint8_t as a template argument for some reason
    int servoIndex = 0;
	int targetPulsewidth = 0;
	bool verbose = false;

	try
	{
		TCLAP::CmdLine cmdLine("Test Program for the servo class", ' ', "0.1");

		std::string description = "Servo index (PWM channel)";
		TCLAP::ValueArg<int> servoIndexArg("i", "index", description, true, servoIndex, "uint8_t");
		cmdLine.add(servoIndexArg);

		description = "Servo target PWM pulsewidth";
		TCLAP::ValueArg<int> targetPulsewidthArg("p", "pulsewidth", description, true, targetPulsewidth, "float");
		cmdLine.add(targetPulsewidthArg);

		description = "verbose output";
		TCLAP::ValueArg<bool> verbosityArg("v", "verbose", description, false, verbose, "bool");
		cmdLine.add(verbosityArg);

		cmdLine.parse(argc, argv);

		servoIndex = servoIndexArg.getValue();
		targetPulsewidth = targetPulsewidthArg.getValue();
		verbose = verbosityArg.getValue();

	}
	catch (TCLAP::ArgException &e)  // catch exceptions
	{ 
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
	}

    std::shared_ptr<i2cPwmController::II2cPwmController> pwmController = std::make_shared<i2cPwmController::CPca9685>(1, 0x40);
	pwmController->setFrequency(50);
	pwmController->setActive(true);

    gpioSetSignalFunc(SIGINT, stopRunning);

    std::array<std::unique_ptr<servo::Servo>, 12> servos = {
       std::make_unique<servo::Servo>("0", pwmController, 0),
       std::make_unique<servo::Servo>("1", pwmController, 1),
       std::make_unique<servo::Servo>("2", pwmController, 2),
       std::make_unique<servo::Servo>("3", pwmController, 3),
       std::make_unique<servo::Servo>("4", pwmController, 4),
       std::make_unique<servo::Servo>("5", pwmController, 5),
       std::make_unique<servo::Servo>("6", pwmController, 6),
       std::make_unique<servo::Servo>("7", pwmController, 7),
       std::make_unique<servo::Servo>("8", pwmController, 8),
       std::make_unique<servo::Servo>("9", pwmController, 9),
       std::make_unique<servo::Servo>("10", pwmController, 10),
       std::make_unique<servo::Servo>("11", pwmController, 11),
    };

    for (size_t i = 0, e = servos.size(); i < e; ++i)
    {
        servos[i]->setAngle(0);
    }

    servos[servoIndex]->setPulsewidth(targetPulsewidth);

    while(sigTermReceived==0)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    pwmController->setActive(false);
}