#include "CDummyI2cPwmController.h"
#include "servo.h"
#include "joint.h"
#include "leg.h"

#include "gtest/gtest.h"

#include <Eigen/Dense>
#include <memory>
#include <array>
#include <math.h>
#include <iostream>

namespace leg
{

class legTest : public ::testing::Test
{
protected:
	void SetUp() override
	{
		pwmController = std::make_shared<i2cPwmController::CDummyI2cPwmController>(1, 0x40);
		servo = { std::make_unique<servo::Servo>("Test 0", pwmController, 0), std::make_unique<servo::Servo>("Test 1", pwmController, 1), std::make_unique<servo::Servo>("Test 2", pwmController, 2)  };
		joint = {	std::make_unique<joint::Joint>("Test 0", std::move(servo[0]), false, M_PI/2, 0), 
				std::make_unique<joint::Joint>("Test 1", std::move(servo[1]), true, M_PI/2, 0), 
				std::make_unique<joint::Joint>("Test 2", std::move(servo[2]), false, 0, 0) };
		leg = std::make_unique<leg::Leg>("Test", std::move(joint), Eigen::Matrix4f::Identity());
	}

	std::shared_ptr<i2cPwmController::II2cPwmController> pwmController;
	std::array<std::unique_ptr<servo::Servo>, 3> servo;
	std::array<std::unique_ptr<joint::Joint>, 3> joint;
	std::unique_ptr<leg::Leg> leg;
};

TEST_F(legTest, pos0)
{
	Eigen::Vector4f testPos;
	testPos << 0.153, 0, 0, 0;
	Eigen::Vector3f expectedPose;
	expectedPose << 0, 0, 0;
	Eigen::Vector3f poseOut = leg->inverseKinematics(testPos);

	for (size_t i = 0; i < 3; ++i)
	{
		EXPECT_NEAR(poseOut[i], expectedPose[i], 1e-3);
	}


	ASSERT_EQ(1, 1);
}


TEST_F(legTest, pos1)
{
	Eigen::Vector4f testPos;
	testPos << 0.078, 0, -0.075, 0;
	Eigen::Vector3f expectedPose;
	expectedPose << 0, 0, M_PI/2;
	Eigen::Vector3f poseOut = leg->inverseKinematics(testPos);

	for (size_t i = 0; i < 3; ++i)
	{
		EXPECT_NEAR(poseOut[i], expectedPose[i], 1e-3);
	}


	ASSERT_EQ(1, 1);
}

TEST_F(legTest, pos2)
{
	Eigen::Vector4f testPos;
	testPos << 0, -0.078, -0.075, 0;
	Eigen::Vector3f expectedPose;
	expectedPose << -M_PI/2, 0, M_PI/2;
	Eigen::Vector3f poseOut = leg->inverseKinematics(testPos);

	for (size_t i = 0; i < 3; ++i)
	{
		EXPECT_NEAR(poseOut[i], expectedPose[i], 1e-3);
	}


	ASSERT_EQ(1, 1);
}

} // namespace leg
