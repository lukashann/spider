# Spooda

Spooda is a 4 legged robot with 3 DoF per leg.

## Software dependencies

I am using the [pigpio](https://abyz.me.uk/rpi/pigpio/) for I2C communication to the servo controller. However, you could implement your own servo class and the rest of the code should work.

For the tests I have implemented a dummy I2C PWM controller that does not rely on pigpio, so the tests can be built and run on any system.

## Compiling

To compile the project on a raspberry pi with pigpio installed
```
git clone https://gitlab.com/lukashann/spider.git
mkdir build
cd build
cmake ..
cmake --build .
```

To compile the tests on any system
To compile the project on a raspberry pi with pigpio installed
```
git clone https://gitlab.com/lukashann/spider.git
mkdir build
cd build
cmake ..
cmake --build . --target testLeg
```

## Hardware

The hardware for this project is taken from [here](https://www.instructables.com/ARDUINO-SPIDER-ROBOT-QUADRUPED/), but instead of an arduino I am using a raspberry pi zero W because of the memory requirements for the algorithms I want to implement. I am using [this](https://www.sparkfun.com/products/15316) I2C PWM controller to run the servos.