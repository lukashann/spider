cmake_minimum_required(VERSION 3.23)

project(Tclap
		LANGUAGES CXX
		DESCRIPTION "TCLAP library as a CMake Interface target"
		VERSION 1.2.5)

add_library(Tclap INTERFACE)

target_include_directories(Tclap 
	INTERFACE 
	${CMAKE_CURRENT_LIST_DIR}
)

