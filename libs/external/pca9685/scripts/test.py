import pi_servo_hat
import time
import sys

def runExample():

    print("\nSparkFun Pi Servo Hat Demo\n")
    mySensor = pi_servo_hat.PiServoHat(0x40)

    mySensor.restart()

    mySensor.set_duty_cycle(0, 0.4)

    while(True):
        time.sleep(1)

runExample()
