#ifndef CDUMMYI2CPWMCONTROLLER_H
#define CDUMMYI2CPWMCONTROLLER_H

#include "II2cPwmController.h"

#include <cstdint>

namespace i2cPwmController
{
class CDummyI2cPwmController : public II2cPwmController
{
public:
	CDummyI2cPwmController(uint8_t i2cBus, uint8_t i2cAddr);
	~CDummyI2cPwmController();

	void setActive(bool active) final;
	void setDutyCycle(uint8_t channel, float dutyCycleIn) final;
	void setPhase(uint8_t channel, float phaseIn) final;
	void setFrequency(float frequency) final;

	void setLogLevel(spdlog::level::level_enum level) final;

};
} // namespace i2cPwmController

#endif // CDUMMYI2CPWMCONTROLLER_H
