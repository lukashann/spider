add_library(i2cPwmController
	INTERFACE
	)

target_include_directories(i2cPwmController
	INTERFACE
	${CMAKE_CURRENT_LIST_DIR}
	)

target_link_libraries(i2cPwmController
	INTERFACE
	spdlog
	)
