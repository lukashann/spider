#ifndef II2CPWMCONTROLLER_H
#define II2CPWMCONTROLLER_H

#include "spdlog/spdlog.h"

#include <cstdint>
#include <memory>

namespace i2cPwmController
{

class II2cPwmController
{
public:
	// sets whether the pwmControllers output is active
	virtual void setActive(bool active) = 0;

	// sets the duty cycle of the specified channel
	// channel: 0-15
	// dutyCycle: 0-1
	virtual void setDutyCycle(uint8_t channel, float dutyCycleIn) = 0;

	// sets the phase of the specified channel
	// channel: 0-15
	// phase: 0-2pi
	virtual void setPhase(uint8_t channel, float phaseIn) = 0;

	// sets the frequency of the pwm output
	// frequency: 24-1526
	virtual void setFrequency(float frequency) = 0;

	virtual void setLogLevel(spdlog::level::level_enum level) = 0;

protected:
	int32_t i2cHandle;

	bool isActive = false;
	float dutyCycle;
	float phase;

	std::shared_ptr<spdlog::logger> logger;

};

} //namespace i2cPwmController

#endif // II2CPWMCONTROLLER_H
