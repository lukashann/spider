#ifndef SERVO_H
#define SERVO_H

#include "II2cPwmController.h"

#include "spdlog/spdlog.h"

#include <cstdint>
#include <math.h>
#include <memory>
#include <string>

namespace servo
{

class Servo
{
public:
	Servo(std::string id, std::shared_ptr<i2cPwmController::II2cPwmController> pwmIn, uint8_t channelIn, uint16_t minPulsewidthIn=544, uint16_t maxPulsewidthIn=2400);
	void setAngle(float angleIn);
	void setPulsewidth(uint16_t pulsewidthIn);
	void setLogLevel(spdlog::level::level_enum level);

private:
	std::shared_ptr<i2cPwmController::II2cPwmController> pwmController;

	const uint8_t channel;

	const uint16_t minPulsewidth; // [uS]
	const uint16_t maxPulsewidth; // [uS]
	inline static constexpr uint16_t pulsePeriod = 20000; // [uS]
	inline static constexpr float minAngle = (-M_PI/2); // [rad]
	inline static constexpr float maxAngle = (M_PI/2); // [rad]

	std::shared_ptr<spdlog::logger> logger;
};

} // namespace servo

#endif //  SERVO_H
