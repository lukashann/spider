#include "servo.h"

#include "spdlog/sinks/stdout_color_sinks.h"

#include <iostream>

namespace servo
{

Servo::Servo(std::string id, std::shared_ptr<i2cPwmController::II2cPwmController> pwmIn, uint8_t channelIn, uint16_t minPulsewidthIn, uint16_t maxPulsewidthIn) : pwmController(pwmIn), channel(channelIn), minPulsewidth(minPulsewidthIn), maxPulsewidth(maxPulsewidthIn)
{
	auto stdoutColorSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        stdoutColorSink->set_level(spdlog::level::trace);
        std::vector<spdlog::sink_ptr> sinks = { stdoutColorSink };
	std::string name = std::string("Servo ") + id;
        logger = std::make_shared<spdlog::logger>(name, sinks.begin(), sinks.end());
        logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%=12n] [%^--%L--%$] %v");
        logger->set_level(spdlog::level::warn);
}

void Servo::setAngle(float angleIn)
{
	float angleRange = maxAngle - minAngle;
	float percentageOfAngleRange = (angleIn - minAngle) /angleRange;

	uint16_t pulsewidthRange = maxPulsewidth - minPulsewidth;
	uint16_t targetPulsewidth = minPulsewidth + static_cast<uint16_t>(percentageOfAngleRange * pulsewidthRange);

	logger->trace("Target angle: {}, Target pulse width: {}", angleIn, targetPulsewidth);

	float dutyCycle = static_cast<float>(targetPulsewidth) / pulsePeriod;

	pwmController->setDutyCycle(channel, dutyCycle);

}

void Servo::setPulsewidth(uint16_t pulsewidthIn)
{
	logger->trace("Pulsewidth set point: {}", pulsewidthIn);
	float dutyCycle = static_cast<float>(pulsewidthIn) / pulsePeriod;
	pwmController->setDutyCycle(channel, dutyCycle);
}

void Servo::setLogLevel(spdlog::level::level_enum level)
{
	logger->set_level(level);
}

} // namespace servo
