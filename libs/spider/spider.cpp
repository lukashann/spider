#include "spider.h"

#include <cmath>

#include "CPca9685.h"
#include "servo.h"
#include "joint.h"

namespace spider
{

Spider::Spider()
{
	// setup pwm controller
	pwmController = std::make_shared<i2cPwmController::CPca9685>(1, 0x40);
	pwmController->setFrequency(50);

	for(uint8_t i = 0; i < 12; ++i)
	{
		pwmController->setDutyCycle(i, 0.075);
	}
	pwmController->setActive(true);

	std::array<uint8_t, 4> basePin = {0, 3, 6, 9};
	std::array<uint8_t, 4> hipPin = {1, 4, 7, 10};
	std::array<uint8_t, 4> kneePin = {2, 5, 8, 11};

	// setup servos
	std::array<std::unique_ptr<servo::Servo>, 4> baseServo = {	std::make_unique<servo::Servo>("Base FR", pwmController, basePin[0]), 
									std::make_unique<servo::Servo>("Base RR", pwmController, basePin[1]), 
									std::make_unique<servo::Servo>("Base RL", pwmController, basePin[2]), 
									std::make_unique<servo::Servo>("Base FL", pwmController, basePin[3])  };
	std::array<std::unique_ptr<servo::Servo>, 4> hipServo = {	std::make_unique<servo::Servo>("Hip FR", pwmController, hipPin[0]), 
									std::make_unique<servo::Servo>("Hip RR", pwmController, hipPin[1]), 
									std::make_unique<servo::Servo>("Hip RL", pwmController, hipPin[2]), 
									std::make_unique<servo::Servo>("Hip FL", pwmController, hipPin[3])  };
	std::array<std::unique_ptr<servo::Servo>, 4> kneeServo = {	std::make_unique<servo::Servo>("Knee FR", pwmController, kneePin[0]), 
									std::make_unique<servo::Servo>("Knee RR", pwmController, kneePin[1]), 
									std::make_unique<servo::Servo>("Knee RL", pwmController, kneePin[2]), 
									std::make_unique<servo::Servo>("Knee FL", pwmController, kneePin[3])  };

	// setup joints
	std::array<std::unique_ptr<joint::Joint>, 4> baseJoint = {	std::make_unique<joint::Joint>("Base FR", std::move(baseServo[0]), false, 0, 0.02),
									std::make_unique<joint::Joint>("Base RR", std::move(baseServo[1]), false, 0, -0.04),
									std::make_unique<joint::Joint>("Base RL", std::move(baseServo[2]), false, 0, 0.12),
									std::make_unique<joint::Joint>("Base FL", std::move(baseServo[3]), false, 0, 0.22) };
	std::array<std::unique_ptr<joint::Joint>, 4> hipJoint = {	std::make_unique<joint::Joint>("Hip FR", std::move(hipServo[0]), false, 0, 0.06),
									std::make_unique<joint::Joint>("Hip RR", std::move(hipServo[1]), true, 0, 0),
									std::make_unique<joint::Joint>("Hip RL", std::move(hipServo[2]), false, 0, 0.18),
									std::make_unique<joint::Joint>("Hip FL", std::move(hipServo[3]), true, 0, -0.08) };
	std::array<std::unique_ptr<joint::Joint>, 4> kneeJoint = {	std::make_unique<joint::Joint>("Knee FR", std::move(kneeServo[0]), true, M_PI_2, -0.27),
									std::make_unique<joint::Joint>("Knee RR", std::move(kneeServo[1]), false, -M_PI_2, 0.17),
									std::make_unique<joint::Joint>("Knee RL", std::move(kneeServo[2]), true, M_PI_2, -0.08),
									std::make_unique<joint::Joint>("Knee FL", std::move(kneeServo[3]), false, -M_PI_2, 0.1) };

	// setup legs
	std::array<std::unique_ptr<joint::Joint>, 3> leg0 = { std::move(baseJoint[0]), std::move(hipJoint[0]), std::move(kneeJoint[0]) };
	std::array<std::unique_ptr<joint::Joint>, 3> leg1 = { std::move(baseJoint[1]), std::move(hipJoint[1]), std::move(kneeJoint[1]) };
	std::array<std::unique_ptr<joint::Joint>, 3> leg2 = { std::move(baseJoint[2]), std::move(hipJoint[2]), std::move(kneeJoint[2]) };
	std::array<std::unique_ptr<joint::Joint>, 3> leg3 = { std::move(baseJoint[3]), std::move(hipJoint[3]), std::move(kneeJoint[3]) };

	std::array<std::array<std::unique_ptr<joint::Joint>, 3>, 4> legJoints = { std::move(leg0), std::move(leg1), std::move(leg2), std::move(leg3) };

	float RzRight = -M_PI_2;
	float RzLeft = M_PI_2;
	float legSeparationX = 0.067;
	float legSeparationY = 0.067;
	Eigen::Matrix4f T0FrontRight;
	T0FrontRight << cos(RzRight), -sin(RzRight), 0, legSeparationX/2,
					sin(RzRight), cos(RzRight), 0, -legSeparationY/2,
					0, 0, 1, 0,
					0, 0, 0, 1;
	Eigen::Matrix4f T0RearRight;
	T0RearRight <<	cos(RzRight), -sin(RzRight), 0, -legSeparationX/2,
					sin(RzRight), cos(RzRight), 0, -legSeparationY/2,
					0, 0, 1, 0,
					0, 0, 0, 1;
	Eigen::Matrix4f T0RearLeft;
	T0RearLeft <<	cos(RzLeft), -sin(RzLeft), 0, -legSeparationX/2,
					sin(RzLeft), cos(RzLeft), 0, legSeparationY/2,
					0, 0, 1, 0,
					0, 0, 0, 1;
	Eigen::Matrix4f T0FrontLeft;
	T0FrontLeft << cos(RzLeft), -sin(RzLeft), 0, legSeparationX/2,
					sin(RzLeft), cos(RzLeft), 0, legSeparationY/2,
					0, 0, 1, 0,
					0, 0, 0, 1;

	leg = {	std::make_unique<leg::Leg>("FR", std::move(legJoints[0]), T0FrontRight), 
			std::make_unique<leg::Leg>("RR", std::move(legJoints[1]), T0RearRight), 
			std::make_unique<leg::Leg>("RL", std::move(legJoints[2]), T0RearLeft), 
			std::make_unique<leg::Leg>("FL", std::move(legJoints[3]), T0FrontLeft) };

	leg[0]->setPos(footContactPoint[0]);
	leg[1]->setPos(footContactPoint[1]);
	leg[2]->setPos(footContactPoint[2]);
	leg[3]->setPos(footContactPoint[3]);

	leg[0]->setLogLevel(spdlog::level::level_enum::trace);
	leg[1]->setLogLevel(spdlog::level::level_enum::trace);
	leg[2]->setLogLevel(spdlog::level::level_enum::trace);
	leg[3]->setLogLevel(spdlog::level::level_enum::trace);
}

void Spider::moveToPose(Eigen::Matrix4f poseIn)
{
	Eigen::Matrix4f targetPoseInv = poseIn.inverse();

	// vectors pointing from the target pose (poseIn) to the foot contact point, aka the target leg pose
	std::array<Eigen::Vector4f, 4> T_target_foot;

	for (size_t i = 0, e = T_target_foot.size(); i!=e; ++i)
	{
		T_target_foot[i] = targetPoseInv * footContactPoint[i];
	}

	leg[0]->setPos(T_target_foot[0]);
	leg[1]->setPos(T_target_foot[1]);
	leg[2]->setPos(T_target_foot[2]);
	leg[3]->setPos(T_target_foot[3]);
}

void Spider::disable()
{
	pwmController->setActive(false);
}

} // namespace spider
