#ifndef SPIDER_H
#define SPIDER_H

#include <array>
#include <memory>
#include <Eigen/Dense>

#include "leg.h"

namespace spider
{

class Spider
{
public:
	Spider();

	void moveToPose(Eigen::Matrix4f poseIn);

	void disable();

private:
	std::shared_ptr<i2cPwmController::II2cPwmController> pwmController;
	std::array<std::unique_ptr<leg::Leg>, 4> leg;
	Eigen::Matrix4f referencePose = Eigen::Matrix4f::Identity();
	std::array<Eigen::Vector4f, 4> footContactPoint = {	Eigen::Vector4f({0.0335, -0.0475, -0.09, 1}),
														Eigen::Vector4f({-0.0335, -0.0475, -0.09, 1}),
														Eigen::Vector4f({-0.0335, 0.0475, -0.09, 1}),
														Eigen::Vector4f({0.0335, 0.0475, -0.09, 1}) };

};

} // namespace spider

#endif // SPIDER_H
