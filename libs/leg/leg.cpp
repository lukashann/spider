#include "leg.h"
#include "utils.cpp"

#include "spdlog/sinks/stdout_color_sinks.h"

#include <cmath>

namespace leg
{

Leg::Leg(std::string id, std::array<std::unique_ptr<joint::Joint>, 3> jointsIn, Eigen::Matrix4f T0In): joint(std::move(jointsIn)),  T0(T0In)
{
	// TODO: check if T0 is singular?
	T0Inv = T0.inverse();

	// set leg to default start position
	joint[0]->setAngle(0);
	joint[1]->setAngle(0);
	joint[2]->setAngle(M_PI/2);

	auto stdoutColorSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        stdoutColorSink->set_level(spdlog::level::trace);
        std::vector<spdlog::sink_ptr> sinks = { stdoutColorSink };
	std::string name = std::string("Leg ") + id;
        logger = std::make_shared<spdlog::logger>(name, sinks.begin(), sinks.end());
        logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%=12n] [%^--%L--%$] %v");
        logger->set_level(spdlog::level::warn);

}

void Leg::setPos(Eigen::Vector4f posIn)
{
	logger->trace("T_0_Foot: {}, {}, {}, {}", posIn[0], posIn[1], posIn[2], posIn[3]);

	TFoot = posIn;

	// turn absolute position (relative to body center) into relative position (relative to leg base)
	// positionIn = T0 * TLeg
	// TLeg = T0Inv * positionIn
	Eigen::Vector4f TLeg = T0Inv * posIn;

	logger->trace("T_Base_Foot: {}, {}, {}, {}", TLeg[0], TLeg[1], TLeg[2], TLeg[3]);
	/*
	logger->trace("T_0_Base: \n{}, {}, {}, {}, \n{}, {}, {}, {}, \n{}, {}, {}, {}, \n{}, {}, {}, {}", 
			T0(0, 0), T0(0, 1), T0(0, 2), T0(0, 3), 
			T0(1, 0), T0(1, 1), T0(1, 2), T0(1, 3), 
			T0(2, 0), T0(2, 1), T0(2, 2), T0(2, 3), 
			T0(3, 0), T0(3, 1), T0(3, 2), T0(3, 3));

	logger->trace("T_Base_0: \n{}, {}, {}, {}, \n{}, {}, {}, {}, \n{}, {}, {}, {}, \n{}, {}, {}, {}", 
			T0Inv(0, 0), T0Inv(0, 1), T0Inv(0, 2), T0Inv(0, 3), 
			T0Inv(1, 0), T0Inv(1, 1), T0Inv(1, 2), T0Inv(1, 3), 
			T0Inv(2, 0), T0Inv(2, 1), T0Inv(2, 2), T0Inv(2, 3), 
			T0Inv(3, 0), T0Inv(3, 1), T0Inv(3, 2), T0Inv(3, 3));
			*/
	// TODO: check if position is actually reachable
	Eigen::Vector3f targetAngles = inverseKinematics(TLeg);
  
	setBaseAngle(targetAngles(0));
	setHipAngle(targetAngles(1));
	setKneeAngle(targetAngles(2));
}

// set position of leg relative to center of body
void Leg::setPos(float x, float y, float z)
{
	setPos(Eigen::Vector4f({x, y, z, 1}));
}

void Leg::setBaseAngle(float baseAngle)
{
	logger->trace("baseAngle: {}", baseAngle);
	joint[0]->setAngle(baseAngle);
}

void Leg::setHipAngle(float hipAngle)
{
	logger->trace("hipAngle: {}", hipAngle);
	joint[1]->setAngle(hipAngle);
}

void Leg::setKneeAngle(float kneeAngle)
{
	logger->trace("kneeAngle: {}", kneeAngle);
	joint[2]->setAngle(kneeAngle);
}

Eigen::Vector3f Leg::inverseKinematics(Eigen::Vector4f positionIn)
{
	float x = positionIn(0);
	float y = positionIn(1);
	float z = positionIn(2);


	logger->trace("x: {}, y: {}, z: {}", x, y, z);

	// TODO: this only works for z-values < 0, need to add code to handle z-values > 0

	// angle of the base servo (the servo that connects the leg to the body)
	float baseAngle = atan2(y, x);

	// leg can't turn into robot body
	constexpr float epsilon = 1e-5;
	if (baseAngle > M_PI_2 + epsilon)
	{
	  	baseAngle -= M_PI;
	}
	else if (baseAngle < -(M_PI_2 + epsilon))
	{
  		baseAngle += M_PI;
	}

	// distance in 3D space between the base of the leg and the foot
	float r = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));

	// distance in 3D space between the hip and the foot
	float s = sqrt(pow((x-lenBase*cos(baseAngle)), 2) + pow((y-lenBase*sin(baseAngle)), 2) + pow(z, 2));

	float tmp = (pow(s, 2) - pow(lenUpperLeg, 2) - pow(lenLowerLeg, 2))/(2*lenUpperLeg*lenLowerLeg);
	tmp = constrain(tmp, -1, 1);
	float kneeAngle = acos(tmp);
	kneeAngle = wrapAngle(kneeAngle);

	float tmp0 = (pow(lenBase, 2) + pow(s, 2) - pow(r, 2))/(2*lenBase*s);
	tmp0 = constrain(tmp0, -1, 1);
	float tmp1 = (pow(lenUpperLeg, 2) + pow(s, 2) - pow(lenLowerLeg, 2))/(2*lenUpperLeg*s);
	tmp1 = constrain(tmp1, -1, 1);
	float hipAngle = M_PI - (acos(tmp0) + acos(tmp1));
	hipAngle = wrapAngle(hipAngle);

	Eigen::Vector3f anglesOut = {baseAngle, hipAngle, kneeAngle};
	return anglesOut;
}

Eigen::Vector4f Leg::getPos()
{
	return TFoot;
}

void Leg::setLogLevel(spdlog::level::level_enum level)
{
	logger->set_level(level);
}

} // namespace leg
