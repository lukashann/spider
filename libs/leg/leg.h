#ifndef LEG_H
#define LEG_H

#include "joint.h"

#include "spdlog/spdlog.h"

#include <Eigen/Dense>
#include <array>
#include <memory>
#include <string>

namespace leg
{

class Leg
{
public:
	Leg(std::string id, std::array<std::unique_ptr<joint::Joint>, 3> jointsIn, Eigen::Matrix4f T0In);

	// set target position of foot w.r.t center of parent body
	// Input: T_0_Foot, 4d vector (x, y, z, 1)
	void setPos(Eigen::Vector4f posIn);
	void setPos(float x, float y, float z);
	
	Eigen::Vector4f getPos();
  
	// computes joint configuration to reach positionIn
	// Input: T_Base_Foot, 4d vector (x, y, z, 1), target position w.r.t. base of leg
	// Output: joint configuration, 3d vector (baseAngle, hipAngle, kneeAngle)
	Eigen::Vector3f inverseKinematics(Eigen::Vector4f positionIn);

	void setLogLevel(spdlog::level::level_enum level);

private:
	std::array<std::unique_ptr<joint::Joint>, 3> joint;

	// Transformation matrix from robot frame to leg frame
	Eigen::Matrix4f T0;
	// Transformation matrix from leg frame to robot frame
	Eigen::Matrix4f T0Inv;

	const float lenBase = 0.027; // base length [m]
	const float lenUpperLeg = 0.051; // upper leg length [m]
	const float lenLowerLeg = 0.075; // lower leg length [m]

	// current foot position relative to robot base
	Eigen::Vector4f TFoot;

	void setBaseAngle(float baseAngle);
	void setHipAngle(float hipAngle);
	void setKneeAngle(float kneeAngle);

	std::shared_ptr<spdlog::logger> logger;
	
};

} // namespace leg

#endif // LEG_H
