#ifndef HELPER_H
#define HELPER_H

float rad2deg(float rad);

float wrapAngle(float val);
float wrapMax(float x, float max);
float wrapMinMax(float x, float min, float max);

template <typename T>
T constrain(T val, double min, double max);

#endif // HELPER_H
