#include "utils.h"

#include <math.h>

float rad2deg(float rad)
{
	return 180 * rad / M_PI;
}

float wrapAngle(float val)
{
	return wrapMinMax(val, -M_PI, M_PI);
}

// wrap x -> [0,max) 
float wrapMax(float x, float max)
{
    // integer math: `(max + x % max) % max` 
    return fmod(max + fmod(x, max), max);
}
// wrap x -> [min,max) 
float wrapMinMax(float x, float min, float max)
{
    return min + wrapMax(x - min, max - min);
}
template <typename T>
T constrain(T val, double min, double max)
{
	if (val < min)
	{
		return min;
	}
	else if (val > max)
	{
		return max;
	}
	
	return val;
}
