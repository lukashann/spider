#include "joint.h"

#include "spdlog/sinks/stdout_color_sinks.h"

#include <iostream>

namespace joint
{

Joint::Joint(std::string id, std::unique_ptr<servo::Servo> servoIn, bool flipAngle, float angleOffsetIn, float angleTrimIn) : servo(std::move(servoIn)), angleFlip(1 - static_cast<uint8_t>(flipAngle)*2), angleOffset(angleOffsetIn), angleTrim(angleTrimIn)
{
	auto stdoutColorSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        stdoutColorSink->set_level(spdlog::level::trace);
        std::vector<spdlog::sink_ptr> sinks = { stdoutColorSink };
	std::string name = std::string("Joint ") + id;
        logger = std::make_shared<spdlog::logger>(name, sinks.begin(), sinks.end());
        logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%=12n] [%^--%L--%$] %v");
        logger->set_level(spdlog::level::warn);
}

void Joint::setAngle(float angle)
{
	currentAngle = angle;
	float correctedAngle = (angle + angleTrim) * angleFlip + angleOffset;
	servo->setAngle(correctedAngle);
	logger->trace("Corrected angle: {}", correctedAngle);
	logger->trace("Set angle: {}, angleTrim: {}, angleFlip: {}, angleOffset: {}", angle, angleTrim, angleFlip, angleOffset);
}

float Joint::getAngle()
{
  return currentAngle;
}

void Joint::setLogLevel(spdlog::level::level_enum level)
{
	logger->set_level(level);
}

} // namespace joint
