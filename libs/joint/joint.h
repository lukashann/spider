#ifndef JOINT_H
#define JOINT_H

#include "servo.h"

#include "spdlog/spdlog.h"

#include <cstdint>
#include <memory>
#include <string>

namespace joint
{

// A wrapper class around a Servo that transforms our SI coordinates into servo coordinates
class Joint
{
public:
	Joint(std::string id, std::unique_ptr<servo::Servo> servoIn, bool flipAngle, float angleOffsetIn, float angleTrimIn);

	void setAngle(float angle);
	float getAngle();
	void setLogLevel(spdlog::level::level_enum level);

private:
	std::unique_ptr<servo::Servo> servo;

	const int angleFlip;
	const float angleOffset;
	const float angleTrim;

	float currentAngle;

	std::shared_ptr<spdlog::logger> logger;
};

} // namespace joint

#endif // JOINT_H
